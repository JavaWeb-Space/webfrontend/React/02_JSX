[TOC]
#### 一、JSX 是什么？
- 1、全称 JavaScript XML(xml+js)
- 2、一种允许 html 和 js 混写的文件，需要 babel.min.js 脚本的支持。
    - a、浏览器不能直接解析 JSX 代码, 需要 babel 转译为纯 JS 的代码才能运行。
    - b、只要用了JSX，都要加上 type="text/babel", 声明需要 babel 来处理。
- 3、用来创建 react 虚拟 DOM (元素)对象。
    - 例如：`var ele = <h1>Hello JSX!</h1>` 
    - 此代码是 js+html 的混写，最终将产生一个 JS 对象。

#### 二、JSX 语法规则
- 1、标签名任意，HTMl 标签或者其他标签。
- 2、标签属性任意，HTMl 标签属性或者其他属性。
- 3、基本规则：
    - a.遇到 <开头的代码, 以标签的语法解析: html 同名标签转换为 html 同名元素, 其它标签需要特别解析。
    - b.遇到以 { 开头的代码，以JS语法解析: 标签中的js代码必须用{ }包含。

#### 三、虚拟 DOM
##### 3.1、虚拟 DOM
- 1、React提供了一些API来创建一种 "特别" 的一般 js 对象。
    - a. ```var element = React.createElement('h1', {id:'myTitle'},'hello')```。
    - b.上面创建的就是一个简单的虚拟 DOM 对象。
- 2、虚拟 DOM 对象最终都会被 React 转换为真实的 DOM。
- 3、我们编码时基本只需要操作 react 的虚拟 DOM 相关数据, react 会转换为真实 DOM 变化而更新界面。

##### 3.2、创建虚拟 DOM (元素)
- 1、纯 JS (一般不用)
```
React.createElement('h1',  {id:'myTitle'},  title);
```
- 2、JSX (直接写 html 标签代码)
```
<h1 id='myTitle'>{title}</h1>
```

##### 3.3、渲染虚拟 DOM (元素)
- 1、语法
```
ReactDOM.render(virtualDOM, containerDOM);
```
第一个参数：纯 js 或jsx创建的虚拟 DOM 对象。
第二个参数：用来包含虚拟 DOM 元素的真实 dom 元素对象(一般是一个 div)。
- 2、作用
将虚拟DOM元素渲染到页面中的真实容器 DOM 中显示。

#### 四、JSX 练习
##### 4.1、JSX 练习1：使用 JSX 显示 I LIKE YOU

##### 4.2、JSX 练习2：动态展示列表数据
